from django.shortcuts import render
from .models import NotTogether
from django.shortcuts import redirect

from Candle import candle_alg
from Candle import to_DataFrame
from normal_algorithm_functions.houses_functions import learn_houses
from normal_algorithm_functions.pairs_func import create_pairs
from normal_algorithm_functions.id_to_names_func import teams_text


def generate_data():
    return {'lol': [1, 2,  3], 'kek': [4, 5, 6]}


def is_there_error(data):
    n = 0
    data = {key: value for key, value in data.items() if value != ''}
    if len(data) < 12:
        return 0
    # for el in data:
    #     if (len(data[el].split('\n')) != n) and (n != 0):
    #         return 1
    #     elif n == 0:
    #         n = len(data[el].split('\n'))
    return -1


def query_to_list(all_pairs):
    return [[pair.first_name, pair.second_name] for pair in all_pairs]


def prepared_data(s, column, i):
    if column in ['age', 'form', 'count', 'h_numbers', 'sport_genre']:
        return int(s)
    elif column == 'names':
        return i
    else:
        return s


def data_preparation(data):
    result = []
    if is_there_error(data) == 1:
        return 'Неправильный формат данных: количество введенных значений в некоторых колонках не совпадает'
    elif is_there_error(data) == 0:
        return 'Неправильный формат данных: не все столбцы заполнены'
    else:
        for column in data:
            if column != 'csrfmiddlewaretoken':
                for i in range(len(data[column].split('\n'))):
                     result_row = []
                     for new_column in data:
                         if new_column != 'csrfmiddlewaretoken':
                            result_row.append(prepared_data(data[new_column].split('\n')[i].strip(), new_column, i))
                     result.append(result_row)
                return to_DataFrame(result)


def main(request):
    return render(request, "separation/index.html")


def candle(request):
    action = '/candle'
    if request.method == 'POST':
        data = data_preparation(request.POST)
        names = [s.strip() for s in request.POST['names'].split('\n')]
        if type(data) == str:
            data_result = {'error': data, 'previous_values': request.POST, 'action': action}
        else:
            they_are_not_together = query_to_list(NotTogether.objects.all())
            result = teams_text(names, candle_alg(4, data))
            data_result = {'result': result, 'action': action}
        return render(request, "separation/separation.html", context=data_result)
    else:
        data_result = {'action': action}
        return render(request, "separation/separation.html", context=data_result)


def pairs(request):
    action = '/pairs'
    if request.method == 'POST':
        columns = ["Id", "Sex", "Course", "Age", "Class", "Count", "H_number", "Physical", "Favgenres", "Favsubjects",
                   "Favsports"]
        data = data_preparation(request.POST)
        names = [s.strip() for s in request.POST['names'].split('\n')]
        if type(data) == str:
            data_result = {'error': data, 'previous_values': request.POST, 'action': action}
        else:
            data.columns = columns
            they_are_not_together = query_to_list(NotTogether.objects.all())
            result = teams_text(names, create_pairs(data, they_are_not_together))
            data_result = {'result': result, 'action': action}
        return render(request, "separation/separation.html", context=data_result)
    else:
        data_result = {'action': action}
        return render(request, "separation/separation.html", context=data_result)


def rooms(request):
    action = '/rooms'
    if request.method == 'POST':
        columns = ["Id", "Sex", "Course", "Age", "Class", "Count", "H_number", "Physical", "Favgenres", "Favsubjects",
                   "Favsports"]
        data = data_preparation(request.POST)
        names = [s.strip() for s in request.POST['names'].split('\n')]
        if type(data) == str:
            data_result = {'error': data, 'previous_values': request.POST, 'action': action}
        else:
            data.columns = columns
            they_are_not_together = query_to_list(NotTogether.objects.all())
            result_men = learn_houses(data[data.Sex == 'male'])
            result_women = learn_houses(data[data.Sex == 'female'])
            data_result = {'result': result_men + result_women, 'action': action}
        return render(request, "separation/separation.html", context=data_result)
    else:
        data_result = {'action': action}
        return render(request, "separation/separation.html", context=data_result)


def tutor(request):
    action = '/tutor'
    if request.method == 'POST':
        data = data_preparation(request.POST)
        if type(data) == str:
            result = {'error': data, 'previous_values': request.POST, 'action': action}
        else:
            they_are_not_together = query_to_list(NotTogether.objects.all())
            result = {'result': generate_data(), 'action': action}
        return render(request, "separation/separation.html", context=result)
    else:
        data = {'action': action}
        return render(request, "separation/separation.html", context=data)


def not_together(request):
    if request.method == 'POST':
        pair = NotTogether()
        pair.first_name = request.POST['firstPerson']
        pair.second_name = request.POST['secondPerson']
        pair.save()
        return_path = request.META.get('HTTP_REFERER', '/')
        return redirect(return_path)
    else:
        all_pairs = NotTogether.objects.all()
        data = {'all_pairs': all_pairs}
        return render(request, "separation/addWhoIsNotTogether.html", context=data)


def delete_not_together(request, id):
    pair = NotTogether.objects.get(pk=id)
    pair.delete()
    return_path = request.META.get('HTTP_REFERER', '/')
    return redirect(return_path)


def edit_not_together(request, id):
    if request.method == 'GET':
        data = {'selected_pair': NotTogether.objects.get(pk=id)}
        return render(request, 'separation/editWhoAreNotTogether.html', context=data)
    else:
        pair = NotTogether.objects.get(pk=id)
        pair.first_name = request.POST['firstPerson']
        pair.second_name = request.POST['secondPerson']
        pair.save()
        return redirect('/add_people_who_are_not_together')
