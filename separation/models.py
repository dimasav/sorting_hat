from django.db import models


class NotTogether(models.Model):
    first_name = models.CharField(max_length=200)
    second_name = models.CharField(max_length=200)

