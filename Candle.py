import numpy as np

import pandas as pd


def to_DataFrame(data):
    return pd.DataFrame(np.array(data), columns=[('a' + str(i)) for i in range(len(data[0]))])


def count_houses(group, member,houses_dict):
    member_house = houses_dict[member]
    number_of_houses = 0
    for i in range(len(group)):
        if (group[i] != 0) and (houses_dict[group[i]] == member_house):
            number_of_houses += 1
    return number_of_houses


def candle_alg(number_of_groups, members):
    n = len(members)
    candle_members = np.array([[0 for x in range(n // number_of_groups + 1)] for y in range(number_of_groups)])
    members = members.iloc[np.argsort(members.a2)]
    members = members.values
    members = to_DataFrame(members)
    members.a0 =members.a0.astype(np.int64)
    members.a5 = members.a5.astype(np.int64)
    #print(members)

    f = 0
    for i in range(n % number_of_groups):
        for j in range(n // number_of_groups + 1):
            candle_members[i][j] = members.a0[f]
            f += 1

    for i in range(number_of_groups - n % number_of_groups):
        for j in range(n // number_of_groups):
            candle_members[i + n % number_of_groups][j] = members.a0[f]
            f += 1

    houses_dict = {}
    for i in range(len(members.a0)):
        houses_dict[members.a0[i]] = members.a6[i]

    for i in range(len(candle_members) - 1, -1, -1):
        for j in range(len(candle_members[i])):
            if (i == 0) and (candle_members[i][j] != 0) and (count_houses(candle_members[i], candle_members[i][j], houses_dict) >= 3):
                for n in range(len(candle_members[i + 1])):
                    if (candle_members[i + 1][n] != 0) and (count_houses(candle_members[i], candle_members[i + 1][n], houses_dict) <= 2):
                        candle_members[i][j], candle_members[i + 1][n] = candle_members[i + 1][n], candle_members[i][j]
                        change = 1
                        break
            elif (candle_members[i][j] != 0) and (count_houses(candle_members[i], candle_members[i][j], houses_dict) >= 3):
                change = 0
                for n in range(len(candle_members[i - 1])):
                    if (candle_members[i - 1][n] != 0) and (count_houses(candle_members[i], candle_members[i - 1][n], houses_dict) <= 2):
                        candle_members[i][j], candle_members[i - 1][n] = candle_members[i - 1][n], candle_members[i][j]
                        change = 1
                        break
                if change == 0 and i != len(candle_members) - 1:
                    for n in range(len(candle_members[i + 1])):
                        if (candle_members[i + 1][n] != 0) and (count_houses(candle_members[i], candle_members[i + 1][n], houses_dict) <= 2):
                            candle_members[i][j], candle_members[i + 1][n] = candle_members[i + 1][n], candle_members[i][j]
                            break

    return candle_members




