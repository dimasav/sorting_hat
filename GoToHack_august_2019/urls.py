"""GoToHack_august_2019 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from separation import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.main),
    path('candle', views.candle),
    path('pairs', views.pairs),
    path('rooms', views.rooms),
    path('tutor', views.tutor),
    path('add_people_who_are_not_together', views.not_together),
    path('not_together/<int:id>/delete', views.delete_not_together),
    path('not_together/<int:id>/edit', views.edit_not_together),
]
