def to_DataFrame(data):
    return pd.DataFrame(np.array(data), columns=[('a' + str(i)) for i in range(len(data[0]))])