import random
import numpy as np
import pandas as pd


def create_pairs(data, not_together):  # data - dataframe, not_together = np.full(len(data) + 1,set())
    pairs = []
    data = data.sample(frac=1)
    score_matrix = np.full((len(data), len(data)),0)
    for i in range(len(data)):
        for j in range(len(data)):
            check_together_i = [data.iloc[i, 0], data.iloc[j, 0]] in not_together
            check_together_j = [data.iloc[j, 0], data.iloc[i, 0]] in not_together
            course_together = data['Course'].iloc[i] == data['Course'].iloc[j]
            house_together = data['H_number'].iloc[j] == data['H_number'].iloc[i]
            score_matrix[i,j] = (course_together + house_together + check_together_i + check_together_j + (i == j)*10)
    for i in range(len(score_matrix)):
        best_id = score_matrix[i].argmin()
        if score_matrix[i, best_id] != 100:
            pairs.append([data.iloc[i, 0], data.iloc[best_id, 0]])
            score_matrix[i, :] = 100
            score_matrix[best_id,:] = 100
            score_matrix[:, i] = 100
            score_matrix[:, best_id] = 100
    return pairs
