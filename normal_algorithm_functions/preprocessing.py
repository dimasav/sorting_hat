def preprocess_data(data):
    for i in range(len(data)):
        subjects = data.loc[i,'Favsubjects'].split(';')
        genres = data.loc[i,'Favgenres'].split(';')
        sports = data.loc[i,'Favsports'].split(';')
        data.loc[i, 'course_' + data.loc[i,"Course"]] = 1
        if data.loc[i,'Sex'] == 'male':
            data.loc[i,'Sex'] = 1
        else:
            data.loc[i,'Sex'] = 0
        for s in subjects:
            data.loc[i,'subject_'+s.strip()] = 1
        for g in genres:
            data.loc[i,'genre_'+g.strip()] = 1
        for s in sports:
            data.loc[i,'sport_'+s.strip()] = 1
    data = data.drop(["Favsubjects", "Favsports", "Favgenres"],axis=1)
    data = data.fillna(0)
    return data