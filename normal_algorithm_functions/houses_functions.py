import random
import numpy as np
import pandas as pd


def func_houses(data, n, k):

    d = []
    data2 = data.copy()
    data2 = data2.drop(["Course"], axis=1)
    data2 = data2.drop(["H_number", 'Sex'], axis=1)
    # data2.a0 = data2.a0.astype(np.int64)
    # data2.a5 = data2.a5.astype(np.int64)
    for i in range(k):
        object_num = int(data2.iloc[int(random.random()*len(data2)), 0])
        cur_object = data2.loc[object_num, "Age":]
        data2 = data2.drop(object_num, axis=0)
        data2["distance"] = 0
        for j in data2.Id:
            data2.loc[int(j), "distance"] = np.sqrt(((data2.loc[int(j), "Age":] - cur_object)**2).sum())
        min1_id = data2.loc[data2.distance.idxmin(), "Id"]
        data2 = data2.drop(min1_id, axis=0)
        min2_id = data2.loc[data2.distance.idxmin(), "Id"]
        data2 = data2.drop(min2_id, axis=0)
        if (len(data2) != 2) & (len(data2) != 3) & (len(data2) != 6) & (len(data2) != 0):
            min3_id = data2.loc[data2.distance.idxmin(), "Id"]
            data2 = data2.drop(min3_id, axis=0)
            a = [object_num, min1_id, min2_id, min3_id]
        else:
            a = [object_num, min1_id, min2_id]
        d.append(a)
    return d


def loss_function(id_list, data):  # лосс-функция для оценки качества
    sum_average = 0
    for i in id_list:
        summa = 0
        for j in i:
            summa += ((i - j)**2).sum()
        average = summa / (len(i)**2)
        sum_average += average
    result = sum_average / len(id_list)
    return result


def learn_houses(data):
    n = len(data)
    k = n // 4 + (n % 4 != 0)
    check0 = func_houses(data, k, n)
    for i in range(50):  # обучаем 10 моделей
        check1 = func_houses(data, k, n)
        if (i == 0) or (loss_function(check1, data) < loss_function(check0, data)):  # если следующая лучше, выбираем её
            check0 = check1
    return check1  # выводим значения
