def id_to_name(names, list_of_ids):  # data - dataframe, list_of_ids - one string of names sep=',', from one np list,
    s = ''
    for i in range(len(list_of_ids) - 1):
        s += names[int(list_of_ids[i])] + ', '
    s += names[int(list_of_ids[len(list_of_ids) - 1])]
    return s


def teams_text(names, teams):
    dict_teams = {}
    for i in range(len(teams)):
        dict_teams['Часть ' + str(i + 1)] = id_to_name(names, teams[i])
    return dict_teams
